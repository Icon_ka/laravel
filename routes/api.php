<?php

use Illuminate\Http\Request;
use App\Http\Controllers\UserController;
use App\Http\Controllers\FileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/


Route::middleware('api_no_auth')->group(function () {
    Route::post('/registration', [UserController::class, 'createUser']);
    Route::post('/authorization', [UserController::class, 'loginUser']);
});

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/files/disk', [FileController::class, 'disk']);
    Route::get('/files/shared', [FileController::class, 'shared']);
    Route::get('/files/{file_id}', [FileController::class, 'download']);
    Route::post('/files', [FileController::class, 'store']);
    Route::patch('/files/{file_id}', [FileController::class, 'update']);
    Route::delete('/files/{file_id}', [FileController::class, 'delete']);
    Route::get('/logout', [UserController::class, 'logoutUser']);
    Route::post('/files/{file_id}/accesses', [FileController::class, 'access']);
    Route::delete('/files/{file_id}/accesses', [FileController::class, 'accessDelete']);
});
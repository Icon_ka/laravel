<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UserController extends Controller // основной контроллер регистрации и авторизации
{

    /**
 * @OA\Post(
 *     path="/api/registration",
 *     summary="Регистрация нового пользователя",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\JsonContent(
 *             @OA\Property(property="first_name", type="string", example="Иван"),
 *             @OA\Property(property="last_name", type="string", example="Иванов"),
 *             @OA\Property(property="email", type="string", format="email", example="user@example.com"),
 *             @OA\Property(property="password", type="string", format="password", example="password123"),
 *             @OA\Property(property="password_confirmation", type="string", format="password", example="password123")
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Успешная регистрация",
 *         @OA\JsonContent(
 *             @OA\Property(property="success", type="boolean"),
 *             @OA\Property(property="message", type="string"),
 *             @OA\Property(property="token", type="string")
 *         )
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description="Ошибка валидации"
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="Ошибка сервера"
 *     )
 * )
 */
    public function createUser(Request $request) {
        try {
            // валидация данных
            $validateUser = Validator::make($request->all(), 
            [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email|unique:users,email',
                // пароль должен быть минимум из 3 символов с одной маленькой и большой буквой и цифрой
                'password' => ['required', 'confirmed', Password::min(3)->letters()->mixedCase()->numbers()],
                'password_confirmation' => 'required',
            ]);

            if($validateUser->fails()){ // если валидация не прошла
                return response()->json([
                    'success' => false,
                    'message' => $validateUser->errors(),
                ], 422);
            }

            $user = User::create([ // создаем пользователя
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password, // Laravel самостоятельно хэширует пароль и заносит его таким в БД
            ]);

            return response()->json([ // при успешной регистрации отправляется токен
                'success' => true,
                'message' => 'Success',
                'token' => 'Bearer ' . $user->createToken("API TOKEN")->plainTextToken // создание токена и последующее сохрание его в БД
            ], 200);

        } catch (\Throwable $th) { // если возникает какая-либо ошибка
            return response()->json([
                'success' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    /**
 * @OA\Post(
 *     path="/api/authorization",
 *     summary="Авторизация пользователя",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\JsonContent(
 *             @OA\Property(property="email", type="string", format="email", example="user@example.com"),
 *             @OA\Property(property="password", type="string", format="password", example="password123")
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Успешная авторизация",
 *         @OA\JsonContent(
 *             @OA\Property(property="success", type="boolean"),
 *             @OA\Property(property="message", type="string"),
 *             @OA\Property(property="token", type="string")
 *         )
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Неверные учетные данные"
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="Ошибка сервера"
 *     )
 * )
 */
    public function loginUser(Request $request){
        try {
            $validateUser = Validator::make($request->all(), 
            [
                'email' => 'required|email',
                'password' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'success' => false,
                    'message' => 'Login failed',
                ], 401);
            }

            if (!Auth::attempt($request->only(['email', 'password']))) { // если такого пользователя нет в БД
                return response()->json([
                    'success' => false,
                    'message' => 'Email and Password does not match with our record.',
                ], 401);
            }

            $user = User::where('email', $request->email)->first();

            return response()->json([
                'success' => true,
                'message' => 'Success',
                'token' => 'Bearer ' . $user->createToken("API TOKEN")->plainTextToken // создается новый токен, который потом сохраняется в БД
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    /**
 * @OA\Post(
 *     path="/api/logout",
 *     summary="Выход из системы",
 *     security={{"bearerAuth":{}}},
 *     @OA\Response(
 *         response=200,
 *         description="Успешный выход",
 *         @OA\JsonContent(
 *             @OA\Property(property="success", type="boolean"),
 *             @OA\Property(property="message", type="string")
 *         )
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="Ошибка сервера"
 *     )
 * )
 */
    public function logoutUser(Request $request) {
        try {
            $request->user()->tokens()->delete(); // удаление токена из базы. Токен, который например лежит в куках, удаляется на стороне клиента

            return response()->json([
                'success' => true,
                'message' => 'Logout',
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Models\File;
use App\Models\UserFile;
use App\Models\User;

/**
 * @OA\Info(
 *   title="My First API",
 *   version="0.1",
 *   description="This is a sample server for the API.",
 *   @OA\Contact(
 *     email="support@example.com"
 *   ),
 *   @OA\License(
 *     name="Apache  2.0",
 *     url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *   )
 * )
 */
class FileController extends Controller
{
    /**
 * @OA\Post(
 *     path="/api/files",
 *     summary="Загрузка файлов",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="multipart/form-data",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="files",
 *                     type="array",
 *                     @OA\Items(type="string", format="binary")
 *                 )
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Успешная загрузка файлов",
 *         @OA\JsonContent(
 *             type="array",
 *             @OA\Items(
 *                 @OA\Property(property="success", type="boolean"),
 *                 @OA\Property(property="message", type="string"),
 *                 @OA\Property(property="name", type="string"),
 *                 @OA\Property(property="url", type="string", format="uri"),
 *                 @OA\Property(property="file_id", type="string")
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description="Ошибка валидации или файлы не найдены"
 *     )
 * )
 */
    public function store(Request $request)
    {
        $files = $request->file('files'); // получаем файлы

        if (!$files) { // если файлы не найдены
            return response()->json([
                'success' => false,
                'message' => 'Files not exists',
            ], 422);
        }

        $message = []; // массив с сообщениями
        foreach ($files as $file) {
            $fileTitle = $file->getClientOriginalName(); // получаем имя файла

            $filesValidation['file'] = $file;
            $validateFile = Validator::make(
                $filesValidation,
                [
                    'file' => 'required|mimes:doc,pdf,docx,zip,jpeg,jpg,png|max:2048', // допустимые типы и размер 2048 КБ (2 МБ)
                ]
            );

            if ($validateFile->fails()) {
                array_push($message, [
                    "success" => "false",
                    "message" => "File not loaded",
                    "name" => $fileTitle,
                ]);

            } else {
                $uploadTitle = explode('.', $fileTitle)[0]; // делим название файла на имя и формат, чтобы можно было между ними вставить переменную (1/2/3/...)
                $uploadExt = explode('.', $fileTitle)[1];

                if (!Storage::disk('local')->exists("/uploads/$fileTitle")) { // проверяем есть ли файл с таким названием
                    $path = Storage::disk('local')->putFileAs('uploads', $file, $fileTitle);
                } else {
                    $count = 1;
                    while (true) {
                        $uploadFile = $uploadTitle . " ($count)" . ".$uploadExt";
                        if (!Storage::disk('local')->exists("/uploads/$uploadFile")) {
                            $path = Storage::disk('local')->putFileAs('uploads', $file, $uploadFile);
                            $fileTitle = $uploadFile;
                            break;
                        }

                        $count = $count + 1; // счетчик, позволяющий нам отмерить точное кол-во одноименных файлов
                    }
                }

                $fileId = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstyvwxyz'), 1, 10); // генерация file_id

                $fileModel = File::create([
                    'file_path' => $path,
                    'file_id' => $fileId,
                    'file_type' => $uploadExt, // сохраняем тип файла
                    'file_name' => $uploadTitle, // сохраняем имя файла
                ]); // загрузка пути и file_id

                UserFile::create([
                    'user_id' => $request->user()->id, // получаем по токену из запроса юзера (Это возможно благодаря Sanctum) и соединяем его с id файла
                    'file_id' => $fileModel->id,
                    'authorOrCoAuthor' => 'author', // задаем, что это автор данного файла
                ]);

                array_push($message, [
                    "success" => "true",
                    "message" => "Success",
                    "name" => $fileTitle,
                    "url" => $path,
                    'file_id' => $fileId,
                ]);
            }
        }

        return response()->json($message, 200); // возвращаются как загруженные, так и не загруженные файлы
    }

    /**
 * @OA\Patch(
 *     path="/api/files/{file_id}",
 *     summary="Обновление имени файла",
 *     @OA\Parameter(
 *         name="file_id",
 *         in="path",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\JsonContent(
 *             @OA\Property(property="name", type="string", example="new_name.pdf")
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Успешное обновление имени файла",
 *         @OA\JsonContent(
 *             @OA\Property(property="success", type="boolean"),
 *             @OA\Property(property="message", type="string")
 *         )
 *     ),
 *     @OA\Response(
 *         response=403,
 *         description="Доступ запрещен"
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description="Ошибка валидации или файл с таким именем уже существует"
 *     )
 * )
 */
    public function update(Request $request, $file_id)
    {
        $file = File::where('file_id', $file_id)->first(); // получаем по сгенерированному id файл attach

        if (!$file)
            abort(404); // если неверный file_id

        $validateName = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validateName->fails()) {
            return response()->json([ // если строка name пуста
                'success' => false,
                'message' => $validateName->errors(),
            ], 422);
        }

        $connection = UserFile::where('file_id', $file->id)->where('user_id', $request->user()->id)->first();

        if (!$connection || $connection->authorOrCoAuthor == 'co-author') {
            return response()->json([
                'success' => false,
                'message' => 'Forbidden for you',
            ], 403);
        }

        $name = preg_replace('/|\(\)[^ a-zа-я\d.]/ui', '', $request->name); // удаляем не нужные символы для нормальной работы путей
        $newFilePath = "uploads/$name" . ".$file->file_type";
        if (!Storage::disk('local')->exists($newFilePath)) {
            Storage::move($file->file_path, $newFilePath); // меняем название файла в хранилище 

            $file->update([
                'file_path' => $newFilePath,
                'file_name' => $name . '.' . $file->file_type,
            ]);

            return response()->json([
                "success" => true,
                'message' => "File renamed",
            ], 200);
        }

        return response()->json([
            'success' => 'false',
            'message' => 'File must be unique',
        ], 422);
    }
/**
 * @OA\Delete(
 *     path="/api/files/{file_id}",
 *     summary="Удаление файла",
 *     @OA\Parameter(
 *         name="file_id",
 *         in="path",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Файл успешно удален",
 *         @OA\JsonContent(
 *             @OA\Property(property="success", type="boolean"),
 *             @OA\Property(property="message", type="string")
 *         )
 *     ),
 *     @OA\Response(
 *         response=403,
 *         description="Доступ запрещен"
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Файл не найден"
 *     )
 * )
 */
    public function delete(Request $request, $file_id)
    {
        $file = File::where('file_id', $file_id)->first();

        if (!$file)
            abort(404);

        $connection = UserFile::where('file_id', $file->id)->where('user_id', $request->user()->id)->first();

        if (!$connection || $connection->authorOrCoAuthor == 'co-author') {
            return response()->json([
                'success' => false,
                'message' => 'Forbidden for you',
            ], 403);
        }

        $file->delete();

        Storage::delete($file->file_path);

        return response()->json([
            "success" => true,
            'message' => "File already deleted",
        ], 200);
    }

    /**
 * @OA\Get(
 *     path="/api/files/{file_id}",
 *     summary="Скачивание файла",
 *     @OA\Parameter(
 *         name="file_id",
 *         in="path",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Файл успешно скачан",
 *         @OA\JsonContent(
 *             @OA\Property(property="success", type="boolean"),
 *             @OA\Property(property="message", type="string")
 *         )
 *     ),
 *     @OA\Response(
 *         response=403,
 *         description="Доступ запрещен"
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Файл не найден"
 *     )
 * )
 */
    public function download(Request $request, $file_id)
    {
        $file = File::where('file_id', $file_id)->first();

        if (!$file)
            abort(404);

        $connection = UserFile::where('file_id', $file->id)->where('user_id', $request->user()->id)->first();

        if (!$connection || !($connection->authorOrCoAuthor == 'author' || $connection->authorOrCoAuthor == 'co-author')) {
            return response()->json([
                'success' => false,
                'message' => 'Forbidden for you',
            ], 403);
        }

        return Storage::download($file->file_path); // возвращается в ответ файл
    }

    /**
 * @OA\Post(
 *     path="/api/files/{file_id}/accesses",
 *     summary="Предоставление доступа к файлу",
 *     @OA\Parameter(
 *         name="file_id",
 *         in="path",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\JsonContent(
 *             @OA\Property(property="email", type="string", format="email", example="user@example.com")
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Доступ успешно предоставлен",
 *         @OA\JsonContent(
 *             type="array",
 *             @OA\Items(
 *                 @OA\Property(property="fullname", type="string"),
 *                 @OA\Property(property="email", type="string"),
 *                 @OA\Property(property="type", type="string")
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response=403,
 *         description="Доступ запрещен"
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description="Ошибка валидации"
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Файл или пользователь не найдены"
 *     )
 * )
 */
    public function access(Request $request, $file_id)
    {
        \Log::info('Access function called with file_id: ' . $file_id);

        $validateEmail = Validator::make(
            $request->all(),
            [
            'email' => 'required|email'
            ]
        );

         if ($validateEmail->fails()) {
        \Log::error('Email validation failed for file_id: ' . $file_id);
        return response()->json([
            'success' => false,
            'message' => $validateEmail->errors(),
        ],  422);
     }

        \Log::info('Email validation passed for file_id: ' . $file_id);
        \Log::info('Email: ' . $request->email);

        $file = File::where('file_id', $file_id)->first();
        $user = User::where('email', $request->email)->first();

        if ($user->id == null || $file->id == null) {
        \Log::info('Null: ' . $file_id,$user);
        }

     if (!($file && $user)) {
        \Log::error('File or user not found for file_id: ' . $file_id);
        abort(404);
     }

        \Log::info('File and user found for file_id: ' . $file_id);

        $connection = UserFile::where('file_id', $file->id)->where('user_id', $request->user()->id)->first();

        if (!$connection || !($connection->authorOrCoAuthor == 'author')) {
        \Log::error('User is not authorized to change access for file_id: ' . $file_id);
        return response()->json([
            'success' => false,
            'message' => 'Forbidden for you',
        ],  403);
        }

        \Log::info('User is authorized to change access for file_id: ' . $file_id);

        if (!UserFile::where('user_id', $user->id)->exists()) {
        UserFile::create([
            'user_id' => $user->id,
            'file_id' => $file->id,
            'authorOrCoAuthor' => 'co-author',
        ]);
        \Log::info('Access granted to user for file_id: ' . $file_id);
        }

        $connections = UserFile::where('file_id', $file->id)->get();

        $users = [];
        foreach ($connections as $connection) {
        $user = User::where('id', $connection->user_id)->first();
        $user->type = $connection->authorOrCoAuthor;
        array_push($users, $user);
        }

        $messages = [];
        foreach ($users as $user) {
        $message = [
            'fullname' => $user->first_name . ' ' . $user->last_name,
            'email' => $user->email,
            'type' => $user->type,
        ];
        array_push($messages, $message);
        }

        \Log::info('Access information retrieved for file_id: ' . $file_id);

        return response()->json([
        "success" => true,
        'message' => $messages,
        ],  200);
    }
/**
 * @OA\Delete(
 *     path="/api/files/{file_id}/accesses",
 *     summary="Удаление доступа к файлу",
 *     @OA\Parameter(
 *         name="file_id",
 *         in="path",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\JsonContent(
 *             @OA\Property(property="email", type="string", format="email", example="user@example.com")
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Доступ успешно удален",
 *         @OA\JsonContent(
 *             type="array",
 *             @OA\Items(
 *                 @OA\Property(property="fullname", type="string"),
 *                 @OA\Property(property="email", type="string"),
 *                 @OA\Property(property="type", type="string")
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response=403,
 *         description="Доступ запрещен"
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description="Ошибка валидации"
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Файл или пользователь не найдены"
 *     )
 * )
 */
    public function accessDelete(Request $request, $file_id)
    {
        $validateEmail = Validator::make(
            $request->all(),
            [
                'email' => 'required|email'
            ]
        );

        if ($validateEmail->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validateEmail->errors(),
            ], 422);
        }

        $file = File::where('file_id', $file_id)->first();
        $user = User::where('email', $request->email)->first();

        if (!($file && $user)) // если либо пользователь, либо файл не найден
            abort(404);

        $connection = UserFile::where('file_id', $file->id)->where('user_id', $request->user()->id)->first();

        if (!$connection || !($connection->authorOrCoAuthor == 'author')) {
            return response()->json([
                'success' => false,
                'message' => 'Forbidden for you',
            ], 403);
        }

        $userFile = UserFile::where('user_id', $user->id)->where('file_id', $file->id)->first();

        if (!$userFile)
            abort(404);

        if ($userFile->authorOrCoAuthor == 'author') { // сам себя владелец удалить не может
            return response()->json([
                'success' => false,
                'message' => 'Forbidden for you',
            ], 403);
        }

        $userFile->delete();

        $connections = UserFile::where('file_id', $file->id)->get();

        $users = [];
        foreach ($connections as $connection) {
            $user = User::where('id', $connection->user_id)->first();

            $user->type = $connection->authorOrCoAuthor;

            array_push($users, $user);
        }

        $messages = [];
        foreach ($users as $user) {
            $message = [
                'fullname' => $user->first_name . ' ' . $user->last_name,
                'email' => $user->email,
                'type' => $user->type,
            ];

            array_push($messages, $message);
        }

        return response()->json([
            "success" => true,
            'message' => $messages,
        ], 200);
    }

    /**
 * @OA\Get(
 *     path="/api/files/disk",
 *     summary="Получение информации о файлах и их доступе",
 *     @OA\Response(
 *         response=200,
 *         description="Успешное получение информации о файлах",
 *         @OA\JsonContent(
 *             type="array",
 *             @OA\Items(
 *                 @OA\Property(property="success", type="boolean"),
 *                 @OA\Property(property="message", type="array",
 *                     @OA\Items(
 *                         @OA\Property(property="file_id", type="string"),
 *                         @OA\Property(property="name", type="string"),
 *                         @OA\Property(property="url", type="string", format="uri"),
 *                         @OA\Property(property="accesses", type="array",
 *                             @OA\Items(
 *                                 @OA\Property(property="fullname", type="string"),
 *                                 @OA\Property(property="email", type="string"),
 *                                 @OA\Property(property="type", type="string")
 *                             )
 *                         )
 *                     )
 *                 )
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response=403,
 *         description="Доступ запрещен"
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Файл не найден"
 *     )
 * )
 */
    public function disk(Request $request)
    {
        $files = $request->user()->files()->get();

        $messages = [];
        foreach ($files as $file) {
            $users = $file->users()->get();

            $accesses = [];
            foreach ($users as $user) {
                $connection = UserFile::where('user_id', $user->id)->where('file_id', $file->id)->first();

                array_push($accesses, [
                    'fullname' => $user->first_name . ' ' . $user->last_name,
                    'email' => $user->email,
                    'type' => $connection->authorOrCoAuthor,
                ]);
            }

            $message = [
                'file_id' => $file->file_id,
                'name' => $file->file_name,
                'url' => $file->file_path,
                'accesses' => $accesses,
            ];

            array_push($messages, $message);
        }

        return response()->json([
            "success" => true,
            'message' => $messages,
        ], 200);
    }

    /**
 * @OA\Get(
 *     path="/api/files/shared",
 *     summary="Получение списка общих файлов",
 *     @OA\Response(
 *         response=200,
 *         description="Успешное получение списка общих файлов",
 *         @OA\JsonContent(
 *             type="array",
 *             @OA\Items(
 *                 @OA\Property(property="file_id", type="string"),
 *                 @OA\Property(property="name", type="string"),
 *                 @OA\Property(property="url", type="string", format="uri")
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response=403,
 *         description="Доступ запрещен"
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Файл не найден"
 *     )
 * )
 */
    public function shared(Request $request)
    {
        $files = $request->user()->files()->get();

        $messages = [];
        foreach ($files as $file) {
            array_push($messages, [
                'file_id' => $file->file_id,
                'name' => $file->file_name . '.' . $file->file_type,
                'url' => $file->file_path,
            ]);
        }

        return response()->json($messages, 200);
    }
}
<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class NoAuthAPIMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->headers->get('Authorization'); // получаем токен из заголовков 

        if ($token) {
            return response()->json([
                'message' => 'Forbidden for you',
            ], 403);
        }

        return $next($request);

    }
}

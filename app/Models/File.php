<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class File extends Model
{
    use HasFactory;

    protected $table = 'files';
    protected $guarded = [];

    public function users() {
        return $this->belongsToMany(User::class, 'user_files', 'file_id', 'user_id');
    }
}
